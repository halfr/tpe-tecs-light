; 2**15-1
@32767
D=A
D=D+A
@full
M=D+1
@SCREEN
D=A
@x
M=D
(LOOP)
@full
D=M
@x
; the content of x is the current screen pointer
A=M
; we put in the ram cell (= screen pointer, M), our value
M=D
@x
M=M+1
@LOOP
0;JMP
