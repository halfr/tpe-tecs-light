#!/usr/bin/env python
#-*- encoding: utf-8 -*-
#
#       chiputils.py
#
#       Copyright (C) 2009 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
""" A command line tool to manipulate chips db """
import readline
import sys
from optparse import OptionParser
from collections import defaultdict

import pydot

from pychip.chip import Chip, pintuple2str
from pychip.db import ChipDB
from utils import do_color

DB = ChipDB()

class PinError(Exception):
    def __init__(self, pin):
        self.pin = pin

class UndefinedPin(PinError):
    def __str__(self):
        return "Undefined pin: %s" % repr(self.pin)

class UninitializedTmpPin(PinError):
    def __str__(self):
        return "Using uninitialized tmp pin as input: %s" % repr(self.pin)

class UninitializedOutPin(PinError):
    def __str__(self):
        return "Using uninitialized out pin as input: %s" % repr(self.pin)

# chip utils helpers

def from_user(what):
    """ Ask something to the user """
    return raw_input(do_color('white', what)).strip()

def info(what):
    """ Diplay an information """
    print do_color('red', what)

def get_chip():
    """ Ask the user the name of a chip and retrieve it """
    chip = None
    while not chip:
        chip_name = from_user('which chip ? ')
        if chip_name == "":
            return None
        try:
            chip = DB[chip_name]
        except KeyError:
            info('wrong name/this chip does not exist, try again')
    return chip

# real controllers

def iload_db():
    """ Interactive load the db """
    db_filename = from_user('where is the db ? [chips16.db] ')
    db_filename = 'chips16.db' if not db_filename else db_filename
    load_db(db_filename)

def load_db(db_filename):
    """ Load the db """
    DB.load(db_filename)
    info('loading db %s' % db_filename)

def save_db():
    """ Save the db """
    DB.save()

def list_chips():
    """ List all the ship in the db """
    for name in sorted(DB.all().keys()):
        print '\t%s' % do_color('green', name)
    info('%s chips in the db' % len(DB.all().items()))

def delete_chip():
    """ Delete a chip for the db """
    chip = get_chip()
    if not chip: return

    del DB[chip.name]
    info('you have to save after that')

def dot_depgraph(mode, chip, edge_labeled=False):
    """ The function that explore a chip """
    n_chips = defaultdict(int)

    def go_down(chip, chip_node, graph, depth=0):
        for chip_part, inp, outp in chip.parts:
            if mode == 'single':
                c_node = pydot.Node(chip_part.name)
            else:
                c_node = pydot.Node('%s %s' % (chip_part.name, n_chips[chip_part.name]))
                n_chips[chip_part.name] += 1

            edge_label = '"%s"' % ''.join('%s←%s ' % (k, pintuple2str(v)) for k, v in inp.items()+outp.items())if edge_labeled else ''

            graph.add_edge(pydot.Edge(chip_node, c_node, label=edge_label))
            depth += 1
            depth = go_down(chip_part, c_node, graph, depth)
        return depth

    graph = pydot.Dot(label=chip.name)
    graph.set_node_defaults(shape='box')

    go_down(chip, pydot.Node(chip.name), graph)
    return (graph, n_chips)

def depgraph():
    """ Create a dependency graph for a chip"""
    mode = from_user('how should same chips be handled? [single] multiple ')
    mode = 'single' if not mode else 'multiple'

    if mode == 'multiple':
        label = from_user('draw edge label ? [y] ')
        label = True if not label or label == 'y' else False
    else:
        label = False

    chip = get_chip()
    if not chip: return

    graph, stats = dot_depgraph(mode, chip, label)

    filename = from_user('save as: [%s.svg] ' % chip.name)
    filename = '%s.svg' % chip.name if not filename else filename

    graph.write_svg(filename)

def depgraph_all():
    """ Create a dependency graph for all the chips """
    mode = from_user('how should same chips handled? [both] single multiple ')
    mode = 'both' if not mode else mode

    for chip_name, chip in DB.all().items():
        if mode == 'single' or mode == 'both':
            info('graphing %s mode: single' % chip_name)
            graph = dot_depgraph('single', chip)
            graph.write_svg('%s_single.svg' % chip_name)
        if mode == 'multiple' or mode == 'both':
            info('graphing %s mode: multiple' % chip_name)
            graph = dot_depgraph('multiple', chip)
            graph.write_svg('%s_multi.svg' % chip_name)

def stats():
    chip = get_chip()
    if not chip: return

    mode = 'multiple'
    label = False

    graph, stats = dot_depgraph(mode, chip, label)
    for k, v in stats.items():
        print '%s | %s' % (k.center(11), v)

def check_all():
    """ Check all chips in the db """
    for chip in DB.all().values():
        check(chip)

def icheck():
    """ Interactive chip check """
    chip = get_chip()
    if not chip: return

    check(chip)

def check(chip):
    """ Check a chip """
    info('Checking %s' % chip.name)
    info('\tChecking pins')
    testpins(chip)

def testpins(chip):
    i = [pin.name for pin in chip.inputs]
    o = [pin.name for pin in chip.outputs]
    t = [pin.name for pin in chip.tmppins]

    parts = chip.parts

    for part in parts:
        for pi in part[1].values():
            if pi[0] not in i:
                if pi[0] in t:
                    print UninitializedTmpPin(pi[0])
                if pi[0] in o:
                    print UninitializedTmpPin(pi[0])
                else:
                    print UndefinedPin(pi[0])
        for po in part[2].values():
            if po[0] in t or po[0] in o: #using tmp pin
                if not po[0] in i: # now that the tmp pin is initialized, add it to the inputs
                    i.append(po[0])
            else:
                print UndefinedPin(po[0])

def create_chip():
    """ Create a chip
    chip inputs/outputs syntax: pin seperated by a space: a b c[16] d[2]
    parts inputs/outputs: single pin: a
    """
    info('chip creation process')

    chip_name    = from_user('the name of the chip: ')
    chip_inputs  = from_user('inputs of %s: ' % chip_name).split()
    chip_outputs = from_user('outputs of %s: ' % chip_name).split()

    info('creating %s = (%s %s)' % (chip_name, chip_inputs, chip_outputs))

    info('creating parts')
    parts = []
    parts_completed = False
    while not parts_completed:
        part_name = from_user('chip of the part: ')

        parts_completed = True if part_name is '' else False

        try:
            part_chip = DB[part_name]
        except KeyError:
            info('this chip does not exist')
            continue

        part_in = {}
        for i in part_chip.inputs:
            part_in_from = from_user('input of %s of %s ' % (i.name, part_name))
            part_in[i.name] = part_in_from
        part_out = {}
        for i in part_chip.outputs:
            part_out_from = from_user('input of %s of %s ' % (i.name, part_name))
            part_out[i.name] = part_out_from

        info('adding part %s(%s %s)' % (part_name, part_in, part_out))
        parts.append((part_chip, part_in, part_out))

    info('creating real chip')
    chip = Chip(chip_name,
               inputs=chip_inputs,
               outputs=chip_outputs,
               parts=parts)

    DB[chip_name] = chip

def test_chip():
    """ Lets test a chip """
    test_again = True
    chip = get_chip()

    while test_again:
        raw = from_user('give input (%s): ' % str(chip.inputs))
        inputs = dict((chip_input.name, chip_input.set_from_int(int(input))) for chip_input, input in zip(chip.inputs, raw.split()))
        outputs = dict((chip_out.name, chip_out) for chip_out in chip.outputs)

        inputs.update(outputs)
        print inputs
        info('processing')
        chip(**inputs)

        for output, pin in outputs.items():
            info('pin %s is now to %s' % (output, pin.value))

        test_again = from_user('test again ? [N] ')

def rebuild():
    """ Rebuild the database from chips stored in pychip/tests.py """
    import pychip.chip16
    reload(pychip.chip16)

    info('reseting the db')
    DB.reset()
    for chip in pychip.chip16.chips:
        DB[chip.name] = chip

    list_chips()

def _quit():
    """ Quit chiputils.py """
    sys.exit(0)

def force():
    """ Obi-Wan Kenobi """
    info('You use the force.')
    info('It fails.')
    quit()

def interactive():
    """ Interactive mode """
    do_index = {'create': create_chip,
          'delete': delete_chip,
          'rebuild': rebuild,
          'list': list_chips,
          'save': save_db,
          'depgraph': depgraph,
          'depgraph_all': depgraph_all,
          'stats': stats,
          'test': test_chip,
          'check': icheck,
          'check_all':check_all,
          'force': force,
          'quit': _quit,
         }
    iload_db()

    info('chips in the db:')
    list_chips()

    while True:
        whattodo = from_user('what do you want to do ? [create] delete rebuild save depgraph depgraph_all stats check check_all test list quit ')
        whattodo = 'create' if not whattodo else whattodo

        if whattodo not in do_index.keys():
            info('what ?')
            continue

        do_index[whattodo]()
    return 0

def main():
    """ Main function """
    parser = OptionParser()
    parser.add_option('-r', '--rebuild', dest='rebuild', action='store_true',
                      help="rebuild the db")

    (options, args) = parser.parse_args()
    if not args:
        interactive()
    else:
        load_db(args[0])

    #import pdb; pdb.set_trace()
    if options.rebuild:
        rebuild()
        save_db()

if __name__ == '__main__':
    main()

