#!/usr/bin/env python2
#-*- encoding: utf-8 -*-
#
#       ghack.py
#
#       Copyright 2010 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import sys

import gtk
import glib

from tecsmul import Computer, bin2bin

class GHack(object):
    next_program_mode = {
                         'asm': ['bin', 'Binary'],
                         'bin': ['bin+', 'Binary+'],
                         'bin+': ['asm', 'Assembly'],
    }

    def __init__(self):
        self.buil_all()
    
        self.speed = 1000
        self.program_mode = 'asm'
        self.running = False
        self.program = []
        self.asm = []
        self.computer = Computer(self.program) 

        self.setup_memory_view()
        self.setup_program_view()

        self.mainwindow.show_all()

    def buil_all(self):
        b = gtk.Builder()
        b.add_from_file('hack.ui')
        b.connect_signals(self)

        self.b = b
        self.mainwindow = b.get_object('mainwindow')
        self.start_b    = b.get_object('start_b')
        self.D_b        = b.get_object('D_b')
        self.A_b        = b.get_object('A_b')
        self.M_b        = b.get_object('M_b')
        self.PC_b       = b.get_object('PC_b')

        
    def setup_program_view(self):
        treeview = self.b.get_object('program_view') # treeview

        self.program_liststore = gtk.ListStore(str, str)
        self.update_program_view()

        tvcolumn_line = gtk.TreeViewColumn('Line')
        treeview.append_column(tvcolumn_line)

        cell_line = gtk.CellRendererText()
        tvcolumn_line.pack_start(cell_line, True)
        tvcolumn_line.add_attribute(cell_line, 'text', 0)
        tvcolumn_line.set_sort_column_id(0)

        tvcolumn_value = gtk.TreeViewColumn('Value')
        treeview.append_column(tvcolumn_value)
        cell_value = gtk.CellRendererText()
        cell_value.set_property('family', 'Monospace')

        tvcolumn_value.pack_start(cell_value, True)
        tvcolumn_value.add_attribute(cell_value, 'text', 1)

        treeview.set_search_column(0)
        treeview.set_model(self.program_liststore)
        
    def setup_memory_view(self):
        treeview = self.b.get_object('memory_view') # treeview

        self.memory_liststore = gtk.ListStore(str, str)
        self.update_memory_view()

        tvcolumn_line = gtk.TreeViewColumn('Line')
        treeview.append_column(tvcolumn_line)

        cell_line = gtk.CellRendererText()
        tvcolumn_line.pack_start(cell_line, True)
        tvcolumn_line.add_attribute(cell_line, 'text', 0)
        tvcolumn_line.set_sort_column_id(0)

        tvcolumn_value = gtk.TreeViewColumn('Value')
        treeview.append_column(tvcolumn_value)
        cell_value = gtk.CellRendererText()
        cell_value.connect('edited', self.on_memory_cell_edited)
        cell_value.set_property('editable', True)
        cell_value.set_property('family', 'Monospace')

        tvcolumn_value.pack_start(cell_value, True)
        tvcolumn_value.add_attribute(cell_value, 'text', 1)

        treeview.set_search_column(0)
        treeview.set_model(self.memory_liststore)

    def update_memory_view(self):
        self.memory_liststore.clear()
        for line, value in enumerate(self.computer.RAM):
            self.memory_liststore.append([line, value])

    def update_program_view(self):
        self.program_liststore.clear()
        if self.program_mode in ['bin', 'bin+']:
            for line, value in enumerate(self.program):
                if line == self.computer.pc:
                    line = "▶ %s" % line

                if self.program_mode == 'bin+':
                    if value.startswith('0'):
                        value = '0 '+value[1:]
                    else:
                        value = '%s %s %s %s %s' % (value[0:1], value[1:3], value[3:10], value[10:13], value[13:16])
                self.program_liststore.append([line, value])
        elif self.program_mode == 'asm':
            delay = 0
            for line, value in enumerate(self.asm):
                line -= delay
                if line == self.computer.pc:
                    line = "▶ %s" % line
                if value.startswith('('):
                    delay += 1
                    line = ''
                self.program_liststore.append([line, value])


    def open_chooser(self):
        chooser = gtk.FileChooserDialog(title="Open a Hack file", action=gtk.FILE_CHOOSER_ACTION_OPEN, buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        chooser.run()

        try:
            path = chooser.get_uri().split('file://')[1]
            self.open_file(path)
        except AttributeError:
            pass
        finally:
            chooser.destroy()

    def open_file(self, path):
        program_file = open(path)
        self.program = [line[:-1] for line in program_file]
        self.computer.ROM = [bin2bin(line[::-1]) for line in self.program] # [::-1] for endianess
        try:
            asm_path = path.split('.')
            asm_path = ''.join(asm_path[:1]) + '.asm'
            asm_file = open(asm_path)
            self.asm = [line[:-1] for line in asm_file]
        except IOError:
            pass

        self.update()

    def start(self):
        self.running = True
        glib.timeout_add(self.speed, self.step)
        self.start_b.set_label("Stop")

    def stop(self):
        self.running = False
        self.start_b.set_label("Start")

    def step(self):
        self.computer.step()
        self.update()
        return self.running

    def update(self):
        self.update_memory_view()
        self.update_program_view()
        self.D_b.set_value(self.computer.CPU.D)
        self.A_b.set_value(self.computer.CPU.A)
        self.M_b.set_value(self.computer.M)
        self.PC_b.set_value(self.computer.pc)

    def reset(self):
        self.computer.reset = True
        self.step()
        self.computer.RAM.clear()
        self.computer.reset = False
        self.update()

    # callbacks

    def gtk_main_quit(self, widget=None):
        gtk.main_quit()

    def on_open_menu_activate(self, widget):
        self.open_chooser()

    def on_open_b_clicked(self, widget):
        self.open_chooser()

    def on_speed_b_value_changed(self, widget):
        resume = self.running
        if self.running:
            self.stop()
        self.speed = (100 - widget.get_value_as_int()) * 10
        if resume:
            self.start()

    def on_start_b_clicked(self, widget):
        if self.running:
            self.stop()
        else:
            self.start()

    def on_step_b_clicked(self, widget):
        self.step()

    def on_reset_b_clicked(self, widget):
        self.reset()

    def on_memory_b_clicked(self, widget):
        pass

    def on_memory_cell_edited(self, cell, path, new_text):
        #import pdb; pdb.set_trace()
        self.computer.RAM[int(path)] = int(new_text)
        self.update_memory_view()

    def on_program_mode_clicked(self, widget):
        self.program_mode, label = self.next_program_mode[self.program_mode]
        widget.set_label(label)
        self.update_program_view()

    def on_D_b_value_changed(self, widget, need_update=False):
        if need_update:
            self.computer.CPU.D = widget.get_value_as_int()
        pass

    def on_A_b_value_changed(self, widget, need_update=False):
        if need_update:
            self.computer.CPU.A = widget.get_value_as_int()
        pass

    def on_M_b_value_changed(self, widget, need_update=False):
        if need_update:
            self.computer.M = widget.get_value_as_int()
        pass

    def on_PC_b_value_changed(self, widget, need_update=False):
        print(need_update)
        if need_update:
            self.computer.pc = widget.get_value_as_int()
            self.update()
        pass

def main():
    app = GHack()
    if len(sys.argv) is 2:
        app.open_file(sys.argv[1])
    gtk.main()
    
    return 0

if __name__ == '__main__': main()

