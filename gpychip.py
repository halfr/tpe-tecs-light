#!/usr/bin/env python2
#-*- encoding: utf-8 -*-
#
#       gpychip.py
#
#       Copyright (C) 2009 Rémi Audebert <quaero.galileo@gmail.com> 
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
import gtk

from pychip.db import ChipDB
from pychip.chip import pintuple2str

class GPychip(object):
    def __init__(self):
        self.build_all()
        self.db = None

        self.chip = None
        self.test_pins_in = {}
        self.test_pins_out = {}
        
        self.mainwindow.show_all()

    def build_all(self):
        b = gtk.Builder()
        b.add_from_file('gpychip.ui')
        b.connect_signals(self)

        self.b = b
        self.mainwindow = b.get_object('mainwindow')
        self.chipname = b.get_object('chipname')
        self.chipinputs = b.get_object('chipinputs')
        self.chipoutputs = b.get_object('chipoutputs')
        self.truthtable = b.get_object('truthtable')
        self.truthtable_expander = b.get_object('truthtable_expander')
        self.parts = b.get_object('parts')
        self.tests = b.get_object('tests')

    def populate_chips(self):
        treeview = self.b.get_object('viewchip') # treeview

        liststore = gtk.ListStore(str)
        for chip_name, chip in self.db.db.items():
            liststore.append([chip_name])

        #treeview = gtk.TreeView(liststore)
        tvcolumn = gtk.TreeViewColumn('Chips')
        treeview.append_column(tvcolumn)
        cell = gtk.CellRendererText()

        tvcolumn.pack_start(cell, True)
        tvcolumn.add_attribute(cell, 'text', 0)

        treeview.set_search_column(0)
        tvcolumn.set_sort_column_id(0)
        treeview.set_reorderable(True)

        treeview.set_model(liststore)

    def update_truthtable(self):
        if self.chip:
            self.truthtable.set_label('<tt>%s</tt>' % self.chip.truth_table())

    # callbacks

    def gtk_main_quit(self, widget=None):
        gtk.main_quit()

    def on_chipdb_chooser_file_set(self, chooser):
        path = chooser.get_uri().split('file://')[1]
        self.db = ChipDB(filename=path)
        self.populate_chips()

    def on_viewchip_row_activated(self, widget, path, column):

        # setup parts
        model = widget.get_model()
        path = model.get_iter(path)
        chip_name = model.get_value(path, 0)

        chip = self.db[chip_name]
        self.chip = chip

        self.chipname.set_text(chip_name)
        self.chipinputs.set_text(' '.join(['%s[%s]' % (i.name, i.buses) for i in chip.inputs]))
        self.chipoutputs.set_text(' '.join(['%s[%s]' % (o.name, o.buses) for o in chip.outputs]))

        parts_box = gtk.VBox()
        parts_box.show()
        for p_chip, p_in, p_out in chip.parts:
            line = gtk.Label()
            line.set_use_markup(True)
            txt_i = ', '.join('<b>%s</b>=<b>%s</b>' % (_i, pintuple2str(_o)) for _i, _o in p_in.items())
            txt_o = ', '.join('<b>%s</b>=<b>%s</b>' % (_i, pintuple2str(_o)) for _i, _o in p_out.items())
            txt = '<b>%s</b>(%s, %s)' % (p_chip.name, txt_i, txt_o)
            line.set_label(txt)
            line.show()
            parts_box.pack_start(line, padding=3)

        c = self.parts.get_children()
        if c:
            self.parts.remove(c[0])
        self.parts.add(parts_box)
       
        # setup test
        self.test_pins_in.clear()
        self.test_pins_out.clear()

        test_lines = gtk.VBox()
        test_lines.pack_start(gtk.Label('Input' if len(chip.inputs) is 1 else 'Inputs'))

        for i in chip.inputs:
            input_line = gtk.HBox()
            input_line.pack_start(gtk.Label('%s[%s]' % (i.name, i.buses)))
            entry = gtk.Entry(max=i.buses)
            entry.set_name('test_entry_%s' % i.name)
            entry.connect('changed', self.update_tests)

            input_line.pack_start(entry)
            test_lines.pack_start(input_line)

            self.test_pins_in[i.name] = (i, entry)

        test_lines.pack_start(gtk.Label('Output' if len(chip.outputs) is 1 else 'Outputs'))
        for i in chip.outputs:
            output_line = gtk.HBox()
            output_line.pack_start(gtk.Label('%s[%s]' % (i.name, i.buses)))
            entry = gtk.Entry(max=i.buses)
            entry.set_name('test_entry_%s' % i)
            entry.set_sensitive(False)
            output_line.pack_start(entry)

            test_lines.pack_start(output_line)

            self.test_pins_out[i.name] = (i, entry)
            
        for w in self.tests.get_children():
            self.tests.remove(w)

        self.tests.add(test_lines)

        self.mainwindow.show_all()

        self.truthtable_expander.set_expanded(False)

    def on_truth_table_expander_activate(self, widget):
        if not widget.get_expanded():
            self.update_truthtable()
    
    def update_tests(self, widget):
        kwargs = {}
        for pin, v in self.test_pins_in.items():
            try:
                kwargs[pin] = v[0].set_from_binint(v[1].get_text())
            except ValueError:
                return
        for pin, v in self.test_pins_out.items():
            kwargs[pin] = v[0]

        self.chip(**kwargs)
        for pin, v in self.test_pins_out.items():
            v[1].set_text(v[0].value[::-1])

    def on_savedb_clicked(self, button):
        if self.db:
            self.db.save()

    def on_quit_clicked(self, button):
        self.gtk_main_quit()

    def on_add_clicked(self, button):
        pass

    def modify_clicked_cb(self, button):
        pass


def main():
    app = GPychip()

    gtk.main()
    return 0

if __name__ == '__main__': main()

