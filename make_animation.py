#!/usr/bin/env python2
from PIL import Image

USE_FLIP = False

FILES = ['asm_animation/00{:0>2}.png'.format(i) for i in range(1, 24)]
OUTPUT = 'animation.asm'

def main():
    last_image = [0]*32*256

    print '(LOOP)'

    #TODO: clear screen

    for i, filename in enumerate(FILES):
        current_image = [0]*32*256

        im = Image.open(filename)
        pix = im.load()
        for x in range(512):
            for y in range(256):
                if pix[x, y] == 0:
                    current_image[x//16+y*32] |= 1 << x % 16

        for cell, word in enumerate(current_image):
            if last_image[cell] == word:
                continue
            else:
                last_image[cell] = word

            if word == 65535:
                print """@32767
                         D=A+1
                         D=D+A"""
            elif word == 32768:
                print """@32767
                         D=A+1"""
            elif word > 32767:
                print """@32767
                         D=A
                         @{}
                         D=D+A""".format(word-32767)
            elif word == 1:
                print """@{}
                         M=1""".format(0x4000+cell)
                continue
            else:
                print """@{}
                         D=A""".format(word)
            print """@{}
                     M=D""".format(0x4000+cell, word)
    if USE_FLIP:
        print """@FLIP
                 M=1"""

    for cell, word in enumerate(current_image):
        if word:
            print """@{}
                     M=0""".format(0x4000+cell)

    print '@LOOP'
    print '0;JMP'

if __name__ == '__main__':
    main()
