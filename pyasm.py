#!/usr/bin/env python3
#-*- encoding: utf-8 -*-
#
#       pyasm.py
#
#       Copyright 2010 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import re
import os
import sys
import argparse

JMP = {'JMP': '111',
       'JLE': '110',
       'JNE': '101',
       'JLT': '100',
       'JGE': '011',
       'JEQ': '010',
       'JGT': '001',
       ''   : '000',
}

DEST = {'AMD': '111',
        'AD' : '110',
        'AM' : '101',
        'A'  : '100',
        'MD' : '011',
        'D'  : '010',
        'M'  : '001',
        ''   : '000',
}

ALU = {'M'  : '1110000',
       '!M' : '1110001',
       '-M' : '1110011',
       'M+1': '1110111',
       'M-1': '1110010',
       'D+M': '1000010',
       'M+D': '1000010',
       'D-M': '1010011',
       'M-D': '1000111',
       'D&M': '1000000',
       'D|M': '1010101',
       '0'  : '0101010',
       '1'  : '0111111',
       '-1' : '0111010',
       'D'  : '0001100',
       'A'  : '0110000',
       '-A' : '0110011', # wait wat
       '!D' : '0001101',
       '!A' : '0110001',
       'D+1': '0011111',
       'A+1': '0110111',
       'D-1': '0001110',
       'A-1': '0110010',
       'D+A': '0000010',
       'D-A': '0010011',
       'A-D': '0011100',
       'D&A': '0000000',
       'D|A': '0010101',
}

DEFAULT_SYMBOLS = {'R0'    : 0,
                   'R1'    : 1,
                   'R2'    : 2,
                   'R3'    : 3,
                   'R4'    : 4,
                   'R5'    : 5,
                   'R6'    : 6,
                   'R7'    : 7,
                   'R8'    : 8,
                   'R9'    : 9,
                   'R10'   : 10,
                   'R11'   : 11,
                   'R12'   : 12,
                   'R13'   : 13,
                   'R14'   : 14,
                   'R15'   : 15,
                   'SP'    : 0,
                   'LCL'   : 1,
                   'ARG'   : 2,
                   'THIS'  : 3,
                   'THAT'  : 4,
                   'FLIP'  : 16383,
                   'SCREEN': 16384,
                   'KBD'   : 24576,
}

def int2bin(n, count=16):
    """returns the binary of integer n, using count number of digits"""
    return "".join([str((n >> y) & 1) for y in range(count-1, -1, -1)])

class Asm2Bin:
    SYMB = re.compile(r'^\((?P<symbol>.+)\).*')
    A = re.compile('.*@(?P<symbol>.+).*') #FIXME: check for //
    # oh shit
    ALU = re.compile('^[^;]*?(?P<dest>[AMD]{1,3})?=?(?P<alu>[-+!&|AMD10]+);?(?P<jmp>J[MPLENGQT]{2})?.*') #FIXME: check for wrong instructions

    def __init__(self, input, output, output_type):
        self.input = list(input)
        self.output = output

        self.symbols = DEFAULT_SYMBOLS
        self.symbol_counter = 16

        self.output_type = output_type

        self.debug = open("debug", "w")

    def assemble(self):
        self.find_symbols()
        for line in self.input:
            line = line.strip()
            self.parse_line(line)

    def find_symbols(self):
        pc = 0

        for line in self.input:
            line = line.strip()
            msymb = self.SYMB.match(line)
            ma    = self.A.match(line)
            malu  = self.ALU.match(line)

            if (line.startswith('//') or line.startswith(';')
                    or line.startswith('#') or '~' in line or not line):
                continue
            elif msymb:
                symbole_name = msymb.group('symbol')

                self.debug.write("({})\n".format(symbole_name))

                if symbole_name in self.symbols:
                    raise ValueError("Multiple definition of symbol: '{}'".format(symbole_name))
                else:
                    self.symbols[symbole_name] = pc
            elif ma or malu:
                self.debug.write("{:4} {}\n".format(pc, line))
                pc += 1
            else:
                raise SyntaxError("Unknown instruction: '{}'".format(line))

    def symbol(self, key):
        try:
            return int(key)
        except:
            pass

        if key in self.symbols:
            return self.symbols[key]
        else:
            return self.new_symbol(key)

    def new_symbol(self, key):
        self.symbols[key] = self.symbol_counter
        self.symbol_counter += 1

        return self.symbol_counter - 1

    def parse_line(self, line):
        line = line.strip()

        msymb = self.SYMB.match(line)
        ma    = self.A.match(line)
        malu  = self.ALU.match(line)

        if (msymb or line.startswith('//') or line.startswith(';')
                or line.startswith('#') or '~' in line or not line):
            return
        elif ma:
            address = self.symbol(ma.group('symbol'))
            instruction = '0{:015b}'.format(address)
        elif malu:
            d = malu.groupdict('')
            alu  = ALU[d['alu']]
            dest = DEST[d['dest']]
            jmp  = JMP[d['jmp']]

            instruction = '111{}{}{}'.format(alu, dest, jmp)
        else:
            raise SyntaxError("Unknown instruction: '{}'".format(line))

        self.write_line(instruction)

    def write_line(self, line):
        if self.output_type == "binary":
            self.output.write(bytes([int(line[0:8], 2), int(line[8:16], 2)]))

        elif self.output_type == "c-hex":
            self.output.write(bytes("0x{:04X},".format(int(line, 2)), "UTF-8"))

        else: # ascii
            self.output.write(bytes(line+'\n', "UTF-8"))

def main():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('input', type=argparse.FileType('r'),
            default=sys.stdin)
    parser.add_argument('-o', dest='output', type=argparse.FileType('wb'),
            default=os.fdopen(1, "wb"))
    parser.add_argument('-t', '--type', choices=["c-hex", "binary", "ascii"],
            default="ascii")

    args = parser.parse_args()

    ASM = Asm2Bin(input=args.input, output=args.output, output_type=args.type)
    ASM.assemble()
    return 0

if __name__ == '__main__':
    main()

