#!/usr/bin/env python
#-*- encoding: utf-8 -*-
#
#       chip.py
#
#       Copyright (C) 2009 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
import re

def pintuple2str(pin_t):
    if pin_t[1] is None:
        return pin_t[0]
    else:
        return '%s[%s]' % pin_t

class Pin(object):

    def __init__(self, name='', buses=1, value=False):
        self.name = name

        if buses is None:
            buses = 1
        self.buses = buses

        if not value:
            self.reset()
        else:
            self._value = value

    def __getitem__(self, key):
        return (self, key)

    @property
    def value(self):
        return ''.join([str(int(i)) for i in self._value])

    @property
    def int_value(self):
        return sum([int(i)*2**n for n, i in enumerate(self._value)])

    def set(self, bus, value):
        self._value[bus] = value

    def set_from_int(self, value):
        array = []
        for i in range(self.buses):
            array.append((value/2**i)%2)
        self._value = array
        return self

    def set_from_binint(self, value):
        i = int(value, 2)
        return self.set_from_int(i)

    def get(self, bus):
        return self._value[bus]

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<Pin%s:%s=%s>' % (self.buses, self.name, self.value)

    def alltrue(self):
        return all(self._value)

    def reset(self):
        self._value = [False] * self.buses


class Pin16(Pin):
    def __init__(self, *args, **kwargs):
        super(Pin16, self).__init__(buses=16, *args, **kwargs)

GRND = Pin('GRND', value=[0,])
V = Pin('V', value=[1,])
GRND16 = Pin16('GRND16', value=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,]) # hello [0]*16
V16 = Pin16('V16', value=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,])

class Chip(object):
    multipin = re.compile('(?P<name>\w+)[[](?P<buses>\d+)[]]')

    otherpins = {'GRND': (GRND, 0), 'GRND16': GRND16, 'V': (V, 0), 'V16': V16}

    @classmethod
    def deftopin(cls, pin):
        m = cls.multipin.match(pin)
        if m:
            name = m.group('name')
            buses = int(m.group('buses'))
        else:
            buses = None
            name = pin
        return (name, buses)

    def __init__(self, name, inputs, outputs, parts, tmppins=()):
        self.name = name

        inputs = [self.deftopin(i) for i in inputs]
        self.inputs = [Pin(name=name, buses=buses) for name, buses in inputs]
        outputs = [self.deftopin(o) for o in outputs]
        self.outputs = [Pin(name=name, buses=buses) for name, buses in outputs]
        tmppins = [self.deftopin(t) for t in tmppins]
        self.tmppins = [Pin(name=name, buses=buses) for name, buses in tmppins]


        self.parts = parts
        self.process_parts()

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<PyChip:%s>' % self.name

    def __call__(self, **mykwargs):
        mykwargs.update(dict([(p.name, p) for p in self.tmppins]))
        mykwargs.update(self.otherpins)

        for chip, inputs, outputs in self.parts:
            kwargs = {}
            for i, p in inputs.items():
                if p[1] is None: # a=a
                    his_arg = mykwargs[p[0]]
                    try:
                        if his_arg.buses is 1:
                            his_arg = his_arg[0]
                    except AttributeError:
                        pass
                else: # a=a[x]
                    his_arg = mykwargs[p[0]][p[1]]
                kwargs[i] = his_arg

            for i, p in outputs.items():
                if p[1] is None:
                    his_arg = mykwargs[p[0]]
                    try: # a=a
                        if his_arg.buses is 1:
                            his_arg = his_arg[0]
                    except AttributeError:
                        pass
                    kwargs[i] = his_arg
                else:
                    kwargs[i] = mykwargs[p[0]][p[1]]
                    
            chip(**kwargs)

    def process_parts(self):
        new_parts = []
        for chip, inputs, outputs in self.parts:
            new_inputs = {}
            new_outputs = {}

            for chip_pin, my_pin in inputs.items():
                pin_tuple = self.deftopin(my_pin)
                new_inputs[chip_pin] = pin_tuple

            for chip_pin, my_pin in outputs.items():
                pin_tuple = self.deftopin(my_pin)
                new_outputs[chip_pin] = pin_tuple

            new_parts.append((chip, new_inputs, new_outputs))
        self.parts = new_parts

    def truth_table(self, multi_in=False):
        """ La preuve que les trucs qui servent
            a rien sont les plus compliques """
        str_output = ''
        dashes = 0
        for io in self.inputs + self.outputs:
            header = io.name.center(max(len(io.name), io.buses)) + '|'
            dashes += len(header)
            str_output += header
        str_output += '\n%s' % ('-'*dashes)

        if multi_in:
            intest_pins = dict((c.name, c) for c in self.inputs)
            outtest_pins = dict((c.name, c) for c in self.outputs)
        else:
            intest_pins = dict((c.name, (c, 0)) for c in self.inputs)
            outtest_pins = dict((c.name, (c, 0)) for c in self.outputs)

        n = 0
        while [i for i in self.inputs if not i.alltrue()]:
            str_output += '\n'
            if multi_in:
                for col, pin in enumerate(intest_pins.values()):
                    pin.set_from_int((n/2**col)%2**pin.buses)
            else:
                for col, (pin, no) in enumerate(intest_pins.values()):
                    pin.set_from_int((n/2**col)%2**pin.buses)

            _pins = {}
            _pins.update(intest_pins)
            _pins.update(outtest_pins)
            self(**_pins)

            for v in self.inputs + self.outputs:
                str_output += v.value.center(max(len(v.name), v.buses)) + '|'

            n += 1
            if multi_in:
                for c in outtest_pins.values():
                    c.reset()
            else:
                for c, no in outtest_pins.values():
                    c.reset()

        for i in self.inputs + self.outputs:
            i.reset()
        return str_output

class NAND_(Chip):
    def __init__(self):
        super(NAND_, self).__init__('NAND', ('a', 'b'), ('out', ), parts=[])

    def __call__(self, a, b, out):
        #print a, b, out
        out[0].set(out[1], not (a[0].get(a[1]) and b[0].get(b[1])))

class NAND3WAY_(Chip):
    def __init__(self):
        super(NAND3WAY_, self).__init__('NAND3WAY', ('a', 'b', 'c'), ('out', ), parts=[])

    def __call__(self, a, b, c, out):
        out[0].set(out[1], not (a[0].get(a[1]) and b[0].get(b[1]) and c[0].get(c[1])))

class MULTIPLY16_(Chip):
    def __init__(self):
        super(MULTIPLY16_, self).__init__('MULTIPLY16', ('a',), ('out[16]', ), parts=[])

    def __call__(self, a, out):
        for i in range(16):
            out.set(i, a[0].get(0))

class SELECT_(Chip):
    def __init__(self):
        super(SELECT_, self).__init__('SELECT', ('a'), ('out', ), parts=[])

    def __call__(self, a, out):
        """ a = (pin, 0) """
        out[0].set(out[1], a[0].get(a[1]))

NAND = NAND_()
NAND3WAY = NAND3WAY_()
SELECT = SELECT_()
MULTIPLY16 = MULTIPLY16_()
