#!/usr/bin/env python
#-*- encoding: utf-8 -*-
#
#       tests.py
#
#       Copyright (C) 2009 Rémi Audebert <quaero.galileo@gmail.com> 
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from pychip.chip import Chip, NAND, NAND3WAY, MULTIPLY16, SELECT


# QUESTION : Mais pourquoi ne pas avoir fait ça en VHDL ?
# RÉPONSE : Le simple fait que j'essaye de comprendre le fonctionnement d'un processeur
# ne justifie donc-t-il pas que j'explore également d'autre champs au lieux de me contenter
# de les utiliser ?

#~ from pychip.db import ChipDB
#~ db = ChipDB('chips.db')

NAND3WAY16 = Chip('NAND3WAY16',
                 inputs=('a[16]', 'b[16]', 'c[16]'),
                 outputs=('out[16]',),
                  parts=[(NAND3WAY, {'a':'a[%s]'%i, 'b':'b[%s]'%i, 'c':'c[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
                )

NAND16 = Chip('NAND16',
              inputs=('a[16]', 'b[16]'),
              outputs=('out[16]', ),
              parts=[(NAND, {'a':'a[%s]' %i, 'b':'b[%s]' %i}, {'out':'out[%s]' %i}) for i in range(16)]
             )

NOT = Chip('NOT',
           inputs=('a',),
           outputs=('out',),
           parts=[(NAND, {'a':'a', 'b':'a'}, {'out':'out'})]
          )
#print NOT.truth_table()

NOT2 = Chip('NOT2',
            inputs=('a[2]',),
            outputs=('out[2]',),
            parts=[(NOT, {'a':'a[0]'}, {'out':'out[0]'}),
                   (NOT, {'a':'a[1]'}, {'out':'out[1]'})]
           )
#~ print NOT2.truth_table(multi_in=True)

NOT16 = Chip('NOT16',
            inputs=('a[16]',),
            outputs=('out[16]',),
            parts=[(NOT, {'a':'a[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
            )
#NOT16.truth_table(multi_in=True)

AND = Chip('AND',
           inputs=('a', 'b'),
           outputs=('out',),
           parts=[(NAND, {'a':'a', 'b':'b'}, {'out':'out'}),
                  (NOT, {'a':'out'}, {'out':'out'})]
          )
#print AND.truth_table()

AND16 = Chip('AND16',
            inputs=('a[16]', 'b[16]',),
            outputs=('out[16]',),
            parts=[(AND, {'a':'a[%s]'%i, 'b':'b[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
            )

AND4WAY = Chip('AND4WAY',
               inputs=('a', 'b' 'c', 'd'),
               outputs=('out',),
               tmppins=('x[2]',),
               parts=[(AND, {'a':'a', 'b':'b'}, {'out':'x[0]'}),
               (AND, {'a':'c', 'b':'d'}, {'out':'x[1]'}),
               (AND, {'a':'x[0]', 'b':'x[1]'}, {'out':'out'})]
               )

AND16WAY = Chip('AND16WAY',
                inputs=('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'),
                outputs=('out', ),
                tmppins=('x[4]', ),
                parts=[(AND4WAY, {'a':'a', 'b':'b', 'c':'c', 'd':'d'}, {'out':'x[0]'}),
                       (AND4WAY, {'a':'e', 'b':'f', 'c':'g', 'd':'h'}, {'out':'x[1]'}),
                       (AND4WAY, {'a':'i', 'b':'j', 'c':'k', 'd':'l'}, {'out':'x[2]'}),
                       (AND4WAY, {'a':'m', 'b':'n', 'c':'o', 'd':'p'}, {'out':'x[3]'}),
                       (AND4WAY, {'a':'x[0]', 'b':'x[1]', 'c':'x[2]', 'd':'x[3]'}, {'out':'out'})
                      ]
                )

OR = Chip('OR',
          inputs=('a', 'b'),
          outputs=('out',),
          tmppins=('na', 'nb'),
          parts=[(NOT, {'a':'a'}, {'out':'na'}),
                 (NOT, {'a':'b'}, {'out':'nb'}),
                 (NAND,{'a':'na', 'b':'nb'}, {'out':'out'})]
         )
#print OR.truth_table()
OR4WAY = Chip('OR4WAY',
               inputs=('a', 'b' 'c', 'd'),
               outputs=('out',),
               tmppins=('x[2]'),
               parts=[(OR, {'a':'a', 'b':'b'}, {'out':'x[0]'}),
                      (OR, {'a':'c', 'b':'d'}, {'out':'x[1]'}),
                      (OR, {'a':'x[0]', 'b':'x[1]'}, {'out':'out'})]
            )

OR16WAY = Chip('OR16WAY',
                inputs=('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'),
                outputs=('out', ),
                tmppins=('x[4]', ),
                parts=[(OR4WAY, {'a':'a', 'b':'b', 'c':'c', 'd':'d'}, {'out':'x[0]'}),
                       (OR4WAY, {'a':'e', 'b':'f', 'c':'g', 'd':'h'}, {'out':'x[1]'}),
                       (OR4WAY, {'a':'i', 'b':'j', 'c':'k', 'd':'l'}, {'out':'x[2]'}),
                       (OR4WAY, {'a':'m', 'b':'n', 'c':'o', 'd':'p'}, {'out':'x[3]'}),
                       (OR4WAY, {'a':'x[0]', 'b':'x[1]', 'c':'x[2]', 'd':'x[3]'}, {'out':'out'})
                      ]
                )

OR16 = Chip('OR16',
            inputs=('a[16]', 'b[16]'),
            outputs=('out[16]',),
            parts=[(OR, {'a':'a[%s]'%i, 'b':'b[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
            )
#~ a = Pin('a', 16, value=[0, 1, 0, 1, 0, 0 ,1, 0, 1, 0, 1, 1, 1, 0, 1, 0])
#~ b = Pin('b', 16, value=[1, 1, 1, 1, 0, 1 ,1, 0, 1, 0, 1, 1, 1, 0, 1, 0])
#~ out = Pin('out', 16)
#~ OR16(a=a, b=b, out=out)
#~ print out.value

XOR = Chip('XOR',
           inputs=('a', 'b'),
           outputs=('out',),
           tmppins=('na', 'nb', 'w1', 'w2'),
           parts=[(NOT, {'a':'a'},            {'out':'na'}),
                  (NOT, {'a':'b'},            {'out':'nb'}),
                  (NAND, {'a':'a', 'b':'nb'},  {'out':'w1'}),
                  (NAND, {'a':'na', 'b':'b'},  {'out':'w2'}),
                  (NAND,  {'a':'w1', 'b':'w2'}, {'out':'out'})]
          )
#print XOR.truth_table()

XOR16 = Chip('XOR16',
             inputs=('a[16]', 'b[16]'),
             outputs=('out[16]',),
             parts=[(XOR, {'a':'a[%s]'%i, 'b':'b[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
            )

NXOR = Chip('NXOR',
             inputs=('a', 'b'),
             outputs=('out',),
             tmppins=('w1', 'w2'),
             parts=[(NAND, {'a':'a', 'b':'b'},  {'out':'w1'}),
                    (OR, {'a':'a', 'b':'b'},    {'out':'w2'}),
                    (NAND, {'a':'w1', 'b':'w2'}, {'out':'out'})]
            #parts=[
            #       (XOR, {'a':'a', 'b':'b'}, {'out':'out'}),
            #       (NOT, {'a':'out'}, {'out':'out'}),
            #]
            )

NXOR16 = Chip('NXOR16',
              inputs=('a[16]', 'b[16]'),
              outputs=('out[16]', ),
              tmppins=(),
              parts=[(NXOR, {'a':'a[%s]' % i, 'b':'b[%s]' % i}, {'out':'out[%s]' % i}) for i in range(16)]
             )
#~ IFATHENB = Chip('IFATHENB',
                         #~ inputs=('a', 'b'),
                         #~ outputs=('out', ),
                         #~ tmppins=('nb',),
                         #~ parts=[(NOT, {'a':'b'}, {'out':'nb'}),
                                #~ (OR, {'a':'a', 'b':'nb'}, {'out':'out'})]
               #~ )
#~ IFATHENB.truth_table()

#~ IFBTHENA = Chip('IFBTHENA',
                           #~ inputs=('a', 'b'),
                           #~ outputs=('out', ),
                           #~ tmppins=('na',),
                           #~ parts=[(NOT, {'a':'a'}, {'out':'na'}),
                                  #~ (OR, {'a':'b', 'b':'na'}, {'out':'out'})]
                           #~ )
#~ IFBTHENA.truth_table()


#~ OR8WAY = db['OR8WAY']

#~ OR8WAY16 = Chip('OR8WAY16',
               #~ inputs=('a[16]', 'b[16]', 'c[16]', 'd[16]', 'e[16]', 'f[16]', 'g[16]', 'h[16]'),
               #~ outputs=('out[16]',),
               #~ parts=[(OR8WAY, {'a':'a[%s]'%i,
                                #~ 'b':'b[%s]'%i,
                                #~ 'c':'c[%s]'%i,
                                #~ 'd':'d[%s]'%i,
                                #~ 'e':'e[%s]'%i,
                                #~ 'f':'f[%s]'%i,
                                #~ 'g':'g[%s]'%i,
                                #~ 'h':'h[%s]'%i}, {'out':'out[%s]'%i}) for i in range(16)]
            #~ )

MUXER = Chip('MUXER',
             inputs=('a', 'b', 'sel'),
             outputs=('out',),
             tmppins=('nsel', 'w1', 'w2'),
             parts=[(NOT, {'a':'sel'}, {'out':'nsel'}),
                    (NAND, {'a':'sel', 'b':'b'}, {'out':'w1'}),
                    (NAND, {'a':'nsel', 'b':'a'}, {'out':'w2'}),
                    (NAND ,{'a':'w1', 'b':'w2'}, {'out':'out'})]
           )
#~ print MUXER.truth_table()
#~ a = Pin('a', value=[1, ])
#~ b = Pin('b')
#~ out = Pin('out')
#~ sel = Pin('sel', value=[1, ])
#~ MUXER(a=a, b=b, sel=sel, out=out)

#~ print out.value

MUXER16 = Chip('MUXER16',
               inputs=('a[16]', 'b[16]', 'sel[1]'),
               outputs=('out[16]',),
               parts=[(MUXER, {'a':'a[%s]'%i, 'b':'b[%s]'%i,
                              'sel':'sel[0]'}, {'out':'out[%s]'%i}) for i in range(16)]
              )
#~ #a = Pin('a', 16, value=[1]*16)
#~ #b = Pin('b', 16)
#~ #sel = Pin('sel', value=[0])
#~ #out = Pin('out', 16)
#~ #MUXER16(a=a, b=b, sel=sel, out=out)
#~ #print out.value

MUXER4WAY = Chip('MUXER4WAY',
                 inputs=('a', 'b', 'c', 'd', 'sel[2]'),
                 outputs=('out',),
                 tmppins=('mux0', 'mux1', 'seltrue'),
                 parts=[(MUXER, {'a':'a', 'b':'b', 'sel':'sel[0]'}, {'out':'mux0'}),
                        (MUXER, {'a':'mux0', 'b':'c', 'sel':'sel[1]'}, {'out':'mux1'}),
                        (AND, {'a':'sel[0]', 'b':'sel[1]'}, {'out':'seltrue'}),
                        (MUXER, {'a':'mux1', 'b':'d', 'sel':'seltrue'}, {'out':'out'})]
                )

#~ print MUXER4WAY.truth_table()

MUXER4WAY16 = Chip('MUXER4WAY16',
                  inputs=('a[16]', 'b[16]', 'c[16]', 'd[16]', 'sel[2]'),
                  outputs=('out[16]', ),
                  parts=[(MUXER4WAY, {'a':'a[%s]'%i,
                                      'b':'b[%s]'%i,
                                      'c':'c[%s]'%i,
                                      'd':'d[%s]'%i,
                                      'sel':'sel'}, {'out':'out[%s]'%i}) for i in range(16)]
                )

#~ MUXER8WAY = Chip('MUXER8WAY',
                 #~ inputs=('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'sel[3]'),
                 #~ outputs=('out', ),
                 #~ tmppins=('mux0', 'mux1'),
                 #~ parts=[(MUXER4WAY, {'a':'a', 'b':'b', 'c':'c', 'd':'d', 'sel':'sel',}, {'out':'mux0'}),
                        #~ (MUXER4WAY, {'a':'e', 'b':'f', 'c':'g', 'd':'h', 'sel':'sel',}, {'out':'mux1'}),
                        #~ (MUXER, {'a':'mux0', 'b':'mux1', 'sel':'sel[2]'}, {'out':'out'})
                #~ ]
#~ )

MUXER8WAY = Chip('MUXER8WAY',
                 inputs=('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'sel[3]'),
                 outputs=('out', ),
                 tmppins=['mux%s' % i for i in range(7)],
                 parts=[(MUXER, {'a':'a', 'b':'b', 'sel':'sel[2]'}, {'out':'mux0'}),
                        (MUXER, {'a':'c', 'b':'d', 'sel':'sel[2]'}, {'out':'mux1'}),
                        (MUXER, {'a':'e', 'b':'f', 'sel':'sel[2]'}, {'out':'mux2'}),
                        (MUXER, {'a':'g', 'b':'h', 'sel':'sel[2]'}, {'out':'mux3'}),
                        (MUXER, {'a':'mux0', 'b':'mux1', 'sel':'sel[1]'}, {'out':'mux4'}),
                        (MUXER, {'a':'mux2', 'b':'mux3', 'sel':'sel[1]'}, {'out':'mux5'}),
                        (MUXER, {'a':'mux4', 'b':'mux5', 'sel':'sel[0]'}, {'out':'out'}),
                 ]
)

#~ a = Pin('a', value=[1, ])
#~ b = Pin('b', value=[1, ])
#~ c = Pin('c')
#~ d = Pin('d')
#~ e = Pin('e')
#~ f = Pin('f')
#~ g = Pin('g')
#~ h = Pin('h')
#~ sel = Pin('sel', buses=3, value=[0, 0, 1])
#~ out = Pin('out')

#~ MUXER8WAY(a=a, b=b, c=c, d=d, e=e, f=f, g=g, h=h, sel=sel, out=out)

#~ print out.value
#print MUXER8WAY.truth_table()

#~ #MUXER8WAY16 = Chip('MUXER8WAY16',
#~ #                 inputs=('a[16]', 'b[16]', 'c[16]', 'd[16]', 'e[16]', 'f[16]', 'g[16]', 'h[16]', 'sel[3]'),
#~ #                 outputs=('out[16]', ),
#~ #                 parts=[(MUXER4WAY16, {'a':'a', 'b':'b', 'c':'c', 'd':'d', 'sel':'sel',}, {'out':'mux0'}),
#~ #                        (MUXER4WAY16, {'a':'e', 'b':'f', 'c':'g', 'd':'h', 'sel':'sel',}, {'out':'mux1'}),
#~ #                        (MUXER16, {'a':'mux0', 'b':'mux1', 'sel':'sel[2]'}, {'out':'out'})
#~ #                 ]
#~ #                )

MUXER8WAY16 = Chip('MUXER8WAY16',
                  inputs=('a[16]', 'b[16]', 'c[16]', 'd[16]', 'e[16]', 'f[16]', 'g[16]', 'h[16]', 'sel[3]'),
                  outputs=('out[16]', ),
                  parts=[(MUXER8WAY, {'a':'a[%s]'%i,
                                      'b':'b[%s]'%i,
                                      'c':'c[%s]'%i,
                                      'd':'d[%s]'%i,
                                      'e':'d[%s]'%i,
                                      'f':'d[%s]'%i,
                                      'g':'d[%s]'%i,
                                      'h':'d[%s]'%i,
                                      'sel':'sel'}, {'out':'out[%s]'%i}) for i in range(16)]
                )

#~ #a = Pin16()
#~ #b = Pin16()
#~ #c = Pin16()
#~ #d = Pin16()
#~ #e = Pin16()
#~ #f = Pin16()
#~ #g = Pin16()
#~ #h = Pin16()
#~ #sel = Pin(buses=3)
#~ #out = Pin16()

#~ #MUXER8WAY16(a=a, b=b, c=c, d=d, e=e, f=f, g=g, h=h, sel=sel, out=out)

#~ #print out.value

DEMUXER = Chip('DEMUXER',
               inputs=('in', 'sel'),
               outputs=('a', 'b'),
               tmppins=('nsel',),
               parts=[(NOT, {'a':'sel'}, {'out':'nsel'}),
                      (AND, {'a':'in', 'b':'nsel'}, {'out':'a'}),
                      (AND, {'a':'in', 'b':'sel'}, {'out':'b'})]
              )
#print DEMUXER.truth_table()

HALF_ADDER = Chip('HALF_ADDER',
                  inputs=('a', 'b'),
                  outputs=('sum', 'carry'),
                  parts=[(XOR, {'a':'a', 'b':'b'}, {'out':'sum'}),
                         (AND, {'a':'a', 'b':'b'}, {'out':'carry'})]
                )

FULL_ADDER = Chip('FULL_ADDER',
                  inputs=('a', 'b', 'c'),
                  outputs=('sum', 'carry'),
                  tmppins=('ab', 'ac', 'abac', 'bc'),
                  parts=[
                      (NAND, {'a':'a', 'b':'b'}, {'out':'ab'}),
                      (NAND, {'a':'a', 'b':'c'}, {'out':'ac'}),
                      (NAND, {'a':'b', 'b':'c'}, {'out':'bc'}),
                      (NAND3WAY, {'a':'ab', 'b':'ac', 'c':'bc'}, {'out':'carry'}),
                      (XOR, {'a':'a', 'b':'b'}, {'out':'sum'}),
                      (XOR, {'a':'sum', 'b':'c'}, {'out':'sum'}),
                  ]
                 )

INC1 = Chip('INC1',
            inputs=('a',),
            outputs=('out', 'carry'),
            parts=[
                (NAND, {'a':'a', 'b':'a'}, {'out':'out'}),
                (SELECT, {'a':'a'}, {'out':'carry'})]
           )

INC8 = Chip('INC8',
            inputs=('a[8]',),
            outputs=('out[8]',),
            tmppins=('carry[8]',),
            parts=[(INC1, {'a':'a[0]'}, {'out':'out[0]', 'carry':'carry[0]'})]+
                 [(HALF_ADDER, {'a':'a[%s]'%i, 'b':'carry[%s]'%(i-1)}, {'sum':'out[%s]'%i, 'carry':'carry[%s]'%i}) for i in range(1, 8)]
           )

ADDER = Chip('ADDER',
             inputs=('a[16]', 'b[16]'),
             outputs=('sum[16]', ),
             tmppins=('carry[16]', ),
             parts=[(HALF_ADDER, {'a':'a[0]', 'b':'b[0]'}, {'sum':'sum[0]', 'carry':'carry[0]'})]+
                   [(FULL_ADDER, {'a':'a[%s]'%i, 'b':'b[%s]'%i, 'c':'carry[%s]'%(i-1)}, {'sum':'sum[%s]'%i, 'carry':'carry[%s]'%i}) for i in range(1, 16)]
             )

XOR0IFZ = Chip('XOR0IFZ',
               inputs=('x[16]', 'sel'),
               outputs=('out[16]',),
               tmppins=(),
               parts=[
                (MUXER16, {'a':'x', 'b':'GRND16', 'sel':'sel'}, {'out':'out'}),
               ]
               )

#Shakespear one day said:
TOXORNOTTOX = Chip('TOXORNOTTOX',
                   inputs=('x[16]','sel'),
                   outputs=('out[16]',),
                   tmppins=('sel16[16]', ),
                   parts=[
                       (MULTIPLY16, {'a':'sel'}, {'out':'sel16'}),
                       (XOR16, {'a':'x', 'b':'sel16'}, {'out':'out'})
                   ]
                  )

LOVEANDHATE = Chip('LOVEANDHATE',
                  inputs=('x[16]', 'z', 'n'),
                  outputs=('out[16]',),
                  tmppins=('n16[16]', 'z16[16]', 'nn16[16]', 'nz16[16]', 'nx[16]', 'znandn[16]', 'nxnandn[16]', 'w1[16]'),
                   parts=[(MULTIPLY16, {'a':'n'}, {'out':'n16'}),
                          (MULTIPLY16, {'a':'z'}, {'out':'z16'}),
                          (NOT16, {'a':'x'}, {'out':'nx'}),
                          (NOT16, {'a':'z16'}, {'out':'nz16'}),
                          (NOT16, {'a':'n16'}, {'out':'nn16'}),
                          (NAND16, {'a':'z16', 'b':'n16'}, {'out':'znandn'}),
                          (NAND16, {'a':'nx', 'b':'n16'}, {'out':'nxnandn'}),
                          (NAND3WAY16, {'a':'x', 'b':'nz16', 'c':'nn16'}, {'out':'w1'}),
                          (NAND3WAY16, {'a':'nxnandn', 'b':'znandn', 'c':'w1'}, {'out':'out'})
                          ]
                  )

_LOVEANDHATE1 = Chip('_LOVEANDHATE1',
                    inputs=('x', 'n', 'z', 'q'),
                    outputs=('out',),
                     tmppins=('qx', 'nx', 'nnx', 'nz', 'nnxnz', 'nnnxnz'),
                     parts=(
                         (NAND, {'a':'x', 'b':'q'}, {'out':'qx'}),
                         (NAND, {'a':'x', 'b':'x'}, {'out':'nx'}),
                         (NAND, {'a':'nx', 'b':'n'}, {'out':'nnx'}),
                         (NAND, {'a':'n', 'b':'z'}, {'out':'nz'}),
                         (NAND, {'a':'nnx', 'b':'nz'}, {'out':'nnxnz'}),
                         (NAND, {'a':'nnxnz', 'b':'nnxnz'}, {'out':'nnnxnz'}),
                         (NAND, {'a':'qx', 'b':'nnnxnz'}, {'out':'out'}),)
                     )

_LOVEANDHATE1_2 = Chip('_LOVEANDHATE1_2',
                    inputs=('x', 'n', 'z'),
                    outputs=('out',),
                    tmppins=('nz', 'xz',),
                    parts=(
                            (NOT, {'a':'z'}, {'out':'nz'}),
                            (AND, {'a':'x', 'b':'nz'}, {'out':'xz'}),
                            (XOR, {'a':'xz', 'b':'n'}, {'out':'out'})
                    )
)

_LOVEANDHATE = Chip('_LOVEANDHATE',
                   inputs=('x[8]', 'n', 'z'),
                   outputs=('out[8]',),
                   tmppins=('nz', 'nn', 'nznn', 'q',),
                   parts=[
                         (NAND, {'a':'z', 'b':'z'}, {'out':'nz'}),
                         (NAND, {'a':'n', 'b':'n'}, {'out':'nn'}),
                         (NAND, {'a':'nz', 'b':'nn'}, {'out':'nznn'}),
                         (NAND, {'a':'nznn', 'b':'nznn'}, {'out':'q'}),
                        ]+
                        [(_LOVEANDHATE1, {'x':'x[%s]'%i, 'z':'z', 'n':'n', 'q':'q'}, {'out':'out[%s]'%i}) for i in range(8)]
                   )

ALU = Chip('ALU',
            inputs=('x[16]', 'y[16]', 'zx', 'zy', 'nx', 'ny', 'f', 'no'),
            outputs=('out[16]', 'zr', 'ng'),
            tmppins=('w1[16]', 'w2[16]', 'w3[16]', 'w4[16]','sumxy[16]','xandy[16]','w5[16]', 'nout[16]','w6[16]', 'w7[16]', 'no16[16]'),
            parts=[
                (LOVEANDHATE, {'x':'x', 'z':'zx', 'n':'nx'}, {'out':'w3'}),
                (LOVEANDHATE, {'x':'y', 'z':'zy', 'n':'ny'}, {'out':'w4'}),
                (ADDER, {'a':'w3', 'b':'w4'}, {'sum':'sumxy'}),
                (AND16, {'a':'w3', 'b':'w4'}, {'out':'xandy'}),
                (MUXER16, {'a':'xandy', 'b':'sumxy', 'sel':'f'}, {'out':'w5'}),
                (MULTIPLY16, {'a':'no'}, {'out':'no16'}),
                (XOR16, {'a':'w5', 'b':'no16'}, {'out':'out'}),
                (NXOR16, {'a':'GRND16', 'b':'out'}, {'out':'w7'}),
                (AND16WAY, {'a':'w7[0]', 'b':'w7[1]', 'c':'w7[2]', 'd':'w7[3]', 'e':'w7[4]', 'f':'w7[5]', 'g':'w7[6]', 'h':'w7[7]', 'i':'w7[8]', 'j':'w7[9]', 'k':'w7[10]', 'l':'w7[11]', 'm':'w7[12]', 'n':'w7[13]', 'o':'w7[14]', 'p':'w7[15]'}, {'out':'zr[0]'}),
                (SELECT, {'a':'out[15]'}, {'out':'ng[0]'})
            ]
          )


#x = Pin16('x')
#y = Pin16('y')

#zx = Pin('zx')
#zy = Pin('zy')
#nx = Pin('nx')
#ny = Pin('ny')
#f = Pin('f')
#no = Pin('no')

#out = Pin16('out')
#zr = Pin('zr')
#ng = Pin('ng')


#ALU(x=x, y=y, zx=zx, zy=zy, nx=nx, ny=ny, f=f, no=no, out=out, zr=zr, ng=ng)

#print out.value
#print zr.value
#print ng.value

DFF = Chip('DFF',
          inputs=('D',),
          outputs=('Q', 'NQ'),
           parts=[(SELECT, {'a': 'D'}, {'out': 'Q'}),
                  (NOT, {'a': 'Q'}, {'out': 'NQ'})]
          )

DIVFREQ2 = Chip('DIVFREQ2',
               inputs=('CLK', ),
               outputs=('Q', 'NQ'),
               parts=[(SELECT, {'a': 'NQ'}, {'out': 'Q'}),
                  (NOT, {'a': 'Q'}, {'out': 'NQ'})]
               )

chips = [
         NAND16,
         NAND3WAY16,
         NOT,
         NOT16,
         AND,
         AND16,
         OR,
         OR16,
         OR16WAY,
         XOR,
         XOR16,
         NXOR,
         NXOR16,
         MUXER,
         MUXER16,
         MUXER4WAY,
         MUXER8WAY,
         MUXER4WAY16,
         MUXER8WAY16,
         DEMUXER,
         HALF_ADDER,
         FULL_ADDER,
         INC1,
         INC8,
         ADDER,
         XOR0IFZ,
         TOXORNOTTOX,
         _LOVEANDHATE1,
         _LOVEANDHATE1_2,
         _LOVEANDHATE,
         LOVEANDHATE,
         ALU,
         DFF,
         DIVFREQ2
]
