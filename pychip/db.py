#!/usr/bin/env python
#-*- encoding: utf-8 -*-
#
#       db.py
#
#       Copyright (C) 2009 Rémi Audebert <quaero.galileo@gmail.com> 
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
import pickle

class ChipDB(object):
    def __init__(self, filename=None):
        self.filename = filename
        self.db = {}

        if filename:
            self.load(filename)

    def __getitem__(self, key):
        return self.db[key]

    def __setitem__(self, key, value):
        if key in self.db:
            print("Warning: key %s already the db" % key)
        self.db[key] = value

    def __delitem__(self, key):
        del self.db[key]

    def load(self, filename=None):
        if not filename:
            filename = self.filename
        else:
            self.filename = filename
        f = open(filename, 'a+')
        try:
            self.db = pickle.load(f)
        except EOFError:
            print("db is empty")
            self.db = {}
        f.close()

    def save(self, filename=None):
        if not filename:
            filename = self.filename
        f = open(filename, 'w')
        pickle.dump(self.db, f)
        f.close()

    def all(self):
        return self.db

    def reset(self):
        self.db.clear()
