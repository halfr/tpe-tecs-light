#!/usr/bin/env python2
# coding: utf8
#
#       tecsmul.py
#
#       Copyright (C) 2011 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
import sys
import argparse

ENABLE_SCREEN = True
ENABLE_PYGAME2 = False
ENABLE_DEBUG = False
ENABLE_DEBUG_RAM_OVERFLOW = False
ENABLE_RECOMPILE_PYTHON = True
ENABLE_PYCHIP = False
ENABLE_FLIP = False

OFFSET_SCREEN = 0x4000

if ENABLE_PYCHIP:
    from pychip.chip import Pin, Pin16
    from pychip.chip16 import ALU

if ENABLE_SCREEN:
    if ENABLE_PYGAME2:
        import pygame2
        import pygame2.sdl.constants
        import pygame2.sdl.video as video
    else:
        import pygame

if ENABLE_DEBUG:
    import readline

def int2bin(n, count=16):
    """returns the binary of integer n, using count number of digits"""
    return "".join([str((n >> y) & 1) for y in range(count-1, -1, -1)])

class Memory(object):
    def __init__(self, data=[], size=110):
        self._data = data
        self._data.extend(0 for i in range(len(data), size))

    def clear(self):
        self._data[i] = [0] * len(self._data)

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __str__(self):
        return str(self._data)

# {{{ SCREEN
class MemoryWithScreen(Memory):
    def __init__(self, *args, **kwargs):
        super(MemoryWithScreen, self).__init__(*args, **kwargs)

        if ENABLE_PYGAME2:
            self._screen = Pygame2Screen()
        else:
            self._screen = PygameScreen()

    def __setitem__(self, key, value):
        if ENABLE_FLIP and key == OFFSET_SCREEN - 1 and value == 1:
            print('flipping')
            self._screen.flip()

        if key >= OFFSET_SCREEN and key <= OFFSET_SCREEN + 256 * 32:
            screen_start = key - OFFSET_SCREEN
            x = screen_start % 32
            y = screen_start // 32
            for i in range(16):
                self._screen.set_pixel(x*16+i, y, value>>i&1)

            if not ENABLE_FLIP:
                self._screen.update((x*16, y, (x+1)*16, y+1))

        try:
            self._data[key] = value
        except IndexError:
            pass

class Pygame2Screen:
    def __init__(self):
        video.init()

        self._screen = video.set_mode(512, 256, 32, pygame2.sdl.constants.HWSURFACE|pygame2.sdl.constants.DOUBLEBUF|pygame2.sdl.constants.HWACCEL)

        self._white = pygame2.Color('white')
        self._black = pygame2.Color('black')

        self._screen.fill(self._white)
        self._screen.flip()

    def set_pixel(self, x, y, color_b):
        color = self._black if color_b else self._white
        self._screen.set_at(x, y, color)

    def flip(self):
        self._screen.flip()

    def update(self, rect):
        self._screen.update(rect)


class PygameScreen:
    def __init__(self):
        pygame.init()

        self._screen = pygame.display.set_mode((512, 256))

        self._white = pygame.Color('white')
        self._black = pygame.Color('black')

        self._screen.fill(self._white)
        pygame.display.flip()

    def set_pixel(self, x, y, color_b):
        color = self._black if color_b else self._white
        self._screen.set_at((x, y), color)

    def flip(self):
        self._screen.flip()

    def update(self, rect):
        pygame.display.update(rect)

# }}}

# {{{ CPU
# {{{ class BaseCPU
class BaseCPU(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.A, self.D, self.M, self.outM, self.writeM, self.addressM, self.pc = 0, 0, 0, 0, 0, 0, 0

    def alu(self, instruction):
        raise NotImplementedError

    def __call__(self, M, instruction, reset):
        if reset:
            self.reset()
            return self.writeM, self.outM, self.addressM, self.pc

        self.M = M
        self.writeM = False
        self.pc += 1

        if instruction & 0x8000: # C-instruction
            dest = instruction & 0x38
            jump = instruction & 0x7

            computed, zr, ng = self.alu(instruction)

            if dest & 0x20:
                self.A = computed
                self.addressM = self.A
            elif dest & 0x10:
                self.D = computed
            elif dest & 0x8:
                self.writeM = True
                self.addressM = self.A
            self.outM = computed

            if jump == 0x0:
                pass
            elif ((jump == 0x1 and not ng) or
                (jump == 0x2 and zr) or
                (jump == 0x3 and (zr or not ng)) or
                (jump == 0x4 and ng) or
                (jump == 0x5 and not zr) or
                (jump == 0x6 and (zr or ng)) or
                (jump == 0x7)):
                self.pc = self.A

        else: # A-instruction
            self.A = instruction
            self.addressM = instruction

        return (self.writeM, self.outM, self.addressM, self.pc)
# }}}

class FakeCPU(BaseCPU):
    def __init__(self, *args, **kwargs):
        pass

    def reset(self):
        pass

    def alu(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        pass

# {{{ class CPU
class CPU(BaseCPU):
    def __init__(self, *args, **kwargs):
        super(CPU, self).__init__(*args, **kwargs)

    def alu(self, instruction):
        a = instruction & 0x1000
        c = instruction & 0xFC0

        if a:
            if c == 0xC00:
                computed = self.M
            elif c == 0xC40:
                computed = ~self.M
            elif c == 0xCC0:
                computed = -self.M
            elif c == 0xDC0:
                computed = self.M + 1
            elif c == 0xC80:
                computed = self.M - 1
            elif c == 0x80:
                computed = self.D + self.M
            elif c == 0x4C0:
                computed = self.D - self.M
            elif c == 0x1C0:
                computed = self.M - self.D
            elif c == 0x000:
                computed = self.D & self.M
            elif c == 0x540:
                computed = self.D | self.M
            else:
                raise ValueError("Unknown instruction")
        else:
            if c == 0xA80:
                computed = 0
            elif c == 0xFC0:
                computed = 1
            elif c == 0xE80:
                computed = -1
            elif c == 0x300:
                computed = self.D
            elif c == 0xC00:
                computed = self.A
            elif c == 0x340:
                computed = ~self.D
            elif c == 0xC40:
                computed = ~self.A
            elif c == 0x3C0:
                computed = -self.D
            elif c == 0xCC0:
                computed = -self.A
            elif c == 0x7C0:
                computed = self.D + 1
            elif c == 0xDC0:
                computed = self.A + 1
            elif c == 0x380:
                computed = self.D - 1
            elif c == 0xC80:
                computed = self.A - 1
            elif c == 0x80:
                computed = self.D + self.A
            elif c == 0x4C0:
                computed = self.D - self.A
            elif c == 0x1C0:
                computed = self.A - self.D
            elif c == 0x0:
                computed = self.D & self.A
            elif c == 0x540:
                computed = self.D | self.A
            else:
                raise ValueError("Unknown instruction")

        return (computed, computed==0, computed<0)
# }}}

# {{{ class PyChipCPU
class PyChipCPU(BaseCPU):
    def __init__(self, *args, **kwargs):
        super(PyChipCPU, self).__init__(*args, **kwargs)
        self.pychip_alu = ALU
        self.x   = Pin16('x')
        self.nx  = Pin('nx')
        self.zx  = Pin('zx')
        self.y   = Pin16('y')
        self.ny  = Pin('ny')
        self.zy  = Pin('zy')
        self.f   = Pin('f')
        self.no  = Pin('no')
        self.out = Pin16('out')
        self.zr  = Pin('zr')
        self.ng  = Pin('ng')

    def alu(self, instruction):
        self.x.set_from_int(self.D)
        if instruction & 0x1000:
            self.y.set_from_int(self.M)
        else:
            self.y.set_from_int(self.A)

        self.zx.set_from_int(bool(instruction & 0b100000000000))
        self.nx.set_from_int(bool(instruction & 0b010000000000))
        self.zy.set_from_int(bool(instruction & 0b001000000000))
        self.ny.set_from_int(bool(instruction & 0b000100000000))
        self.f.set_from_int(bool(instruction  & 0b000010000000))
        self.no.set_from_int(bool(instruction & 0b000001000000))

        #print "ALU(x=%s, y=%s, zx=%s, zy=%s, nx=%s, ny=%s, f=%s, no=%s)" % (self.x.value, self.y.value, self.zx.value, self.zy.value, self.nx.value, self.ny.value, self.f.value, self.no.value)

        self.pychip_alu(x=self.x, y=self.y, zx=self.zx, zy=self.zy, nx=self.nx, ny=self.ny, f=self.f, no=self.no, out=self.out, zr=self.zr, ng=self.ng)
        #print "out=%s zr=%s, ng=%s" % (self.out.value, self.zr.value, self.ng.value)
        #import pdb; pdb.set_trace()
        return (self.out.int_value, self.zr.int_value, self.ng.int_value)
# }}}
# }}}

# {{{ Computer
# {{{ class Computer
class Computer(object):
    def __init__(self, program):

        self.ROM = program
        if ENABLE_PYCHIP:
            self.CPU = PyChipCPU()
        elif ENABLE_RECOMPILE_PYTHON:
            self.CPU = FakeCPU()
        else:
            self.CPU = CPU()

        if ENABLE_SCREEN:
            self.RAM = MemoryWithScreen()
        else:
            self.RAM = Memory()
        self.pc = 0
        self.M = 0
        self.reset = 0

    def step(self):
        instruction = self.ROM[self.pc]
        writeM, outM, addressM, pc = self.CPU(self.M, instruction, self.reset)
        try:
            if writeM:
                self.RAM[addressM] = outM
            outM = self.RAM[addressM]
        except IndexError:
            if ENABLE_DEBUG_RAM_OVERFLOW:
                print('Address {:x}h is out of RAM'.format(addressM))
            else:
                pass
        self.pc = pc
        self.M = outM

    def run(self):
        while True:
            self.step()
# }}}

# {{{ class DebugComputer
class DebugComputer(Computer):
    def __init__(self, *args, **kwargs):
        super(DebugComputer, self).__init__(*args, **kwargs)

        self._alive = True

        self._breakpoints_pc = {}
        self._breakpoints_step = []
        self._debug_continue = False

    def cont(self):
        self._debug_continue = True

    def shell(self):
        while not self._debug_continue:
            code = ''
            multiline = True
            indent = 0

            while multiline:
                line = input('[{}] {}'.format(self.pc, ' '*indent*4))
                code += indent * ' ' + line
                if line.endswith('\\'):
                    code += '\n'
                    indent += 1
                else:
                    multiline = False

            try:
                exec(code, globals(), dict(cont=self.cont, bp=self.add_breakpoint, step=self.step, die=self.die, **vars(self)))
            except Exception as e:
                print(e)

    def add_breakpoint_pc(self, pc, callback=None):
        if not callback:
            callback = DebugComputer.shell
        try:
            self._breakpoints_pc[pc].append(callback)
        except KeyError:
            self._breakpoints_pc[pc] = [callback]

    def add_breakpoint_step(self, callback):
        self._breakpoints_step.append(callback)

    def die(self):
        self._alive = False

    def run(self):
        while self._alive:
            for cb in self._breakpoints_step:
                cb(self)
            if self.pc in self._breakpoints_pc:
                for cb in self._breakpoints_pc[self.pc]:
                    cb(self)
            self.step()

# {{{ Debug functions
def watch_ram(address):
    _last_value = None

    def _break_point(computer):
#        nonlocal _last_value

        cpu = computer.CPU
        if cpu.addressM == address and cpu.M != _last_value:
            _last_value = cpu.M
            print('[{:d}]={:d}'.format(cpu.addressM, cpu.M))

    return _break_point

def pc_is(address):

    def _pc_is(computer):
        return computer.pc == address

    return _pc_is

def die_when(predicate):

    def _die_when(computer):
        if predicate(computer):
            computer.die()

    return _die_when
# }}}
# }}}

# {{{ class RecompilePythonComputer
class RecompilePythonComputer(Computer):
    _alu = {0b1110000: 'M',
            0b1110001: '~M',
            0b1110011: '-M',
            0b1110111: 'M+1',
            0b1110010: 'M-1',
            0b1000010: 'D+M',
            0b1010011: 'D-M',
            0b1000111: 'M-D',
            0b1000000: 'D&M',
            0b1010101: 'D|M',
            0b0101010: '0',
            0b0111111: '1',
            0b0111010: '-1',
            0b0001100: 'D',
            0b0110000: 'A',
            0b0001101: '~D',
            0b0110001: '~A',
            0b0001111: '-D',
            0b0110011: '-A',
            0b0011111: 'D+1',
            0b0110111: 'A+1',
            0b0001110: 'D-1',
            0b0110010: 'A-1',
            0b0000010: 'D+A',
            0b0010011: 'D-A',
            0b0000111: 'A-D',
            0b0000000: 'D&A',
            0b0010101: 'D|A'}

    # NEVAR FORGET:
    # :'<,'>s/\([^:]*\):\(.*\)A\(.*\)/\1:\2self.A\3
    # :'<,'>s/\([^:]*\):\(.*\)N\(.*\)/\1:\2self.M\3
    # :'<,'>s/\([^:]*\):\(.*\)D\(.*\)/\1:\2self.D\3
    _alu_python = {'M': 'self.RAM[self.A]',
                   '~M': '~self.RAM[self.A]',
                   '-M': '-self.RAM[self.A]',
                   'M+1': 'self.RAM[self.A] + 1',
                   'M-1': 'self.RAM[self.A] - 1',
                   'D+M': 'self.D + self.RAM[self.A]',
                   'D-M': 'self.D - self.RAM[self.A]',
                   'M-D': 'self.RAM[self.A] - self.D',
                   'D&M': 'self.D & self.RAM[self.A]',
                   'D|M': 'self.D | self.RAM[self.A]',
                   '0': '0',
                   '1': '1',
                   '-1': '-1',
                   'D': 'self.D',
                   'A': 'self.A',
                   '~D': '~self.D',
                   '~A': '~self.A',
                   '-D': '-self.D',
                   '-A': '-self.A',
                   'D+1': 'self.D + 1',
                   'A+1': 'self.A + 1',
                   'D-1': 'self.D - 1',
                   'A-1': 'self.A - 1',
                   'D+A': 'self.D + self.A',
                   'D-A': 'self.D - self.A',
                   'A-D': 'self.A - self.D',
                   'D&A': 'self.D & self.A',
                   'D|A': 'self.D | self.A'}

    _jump = {0b0: 'null',
             0b1: 'JGT',
             0b10: 'JEQ',
             0b11: 'JGE',
             0b100: 'JLT',
             0b101: 'JNE',
             0b110: 'JLE',
             0b111: 'JMP'}

    _jump_python = {'null': False,
                    'JGT': '>',
                    'JEQ': '==',
                    'JGE': '>=',
                    'JLT': '<',
                    'JNE': '!=',
                    'JLE': '<=',
                    'JMP': True}

    _dest = {0b1: 'M',
             0b10: 'D',
             0b100: 'A'}

    _dest_python = {'M': 'self.RAM[self.A]',
                    'D': 'self.D',
                    'A': 'self.A'}

    def __init__(self, *args, **kwargs):
        super(RecompilePythonComputer, self).__init__(*args, **kwargs)

        # self.M is not used
        self.A = None
        self.A_known = False
        self.D = None

        self._compiled_code = []

        self._analyse()

    def _analyse(self):
        for lineno, instruction in enumerate(self.ROM):
            if instruction & 0x8000: # C-instruction
                sources = []

                dests = [self._dest_python[value] for DEST, value in self._dest.items() if DEST & instruction >> 3 & 0x7]

                if 'self.A' in dests:
                    self.A_known = None

                jump = self._jump_python[self._jump[instruction & 0x7]]
#                import pdb; pdb.set_trace()
                alu = self._alu_python[self._alu[instruction >> 6 & 0x7F]]

                if dests:
                    sources.append('{dest} = {alu}'.format(dest=dests[0], alu=alu))
                if len(dests) > 1:
                    for dest in dests[1:]:
                        sources.append('{dest} = {computed}'.format(dest=dest, computed=dests[0]))

                if jump is True:
                    if self.A_known == 18:
                        sources.append('sys.exit(0)')
                    else:
                        sources.append('self.pc = self.A')
                elif jump is not False:
                    sources.append('self.pc = self.A if {dest} {cmp} 0 else self.pc + 1'.format(dest=dests[0] if dests else alu, cmp=jump))
                else:
                    sources.append('self.pc += 1')

                source = '; '.join(sources)

            else: # A-instruction
                source = 'self.A = {0}; self.pc += 1'.format(instruction)
                self.A_known = instruction

#            print(source + ' #{}'.format(lineno))
            code = compile(source, str(lineno), 'exec')

            self._compiled_code.append(code)

    def step(self):
        exec(self._compiled_code[self.pc])
# }}}
# }}}

def main():
    parser = argparse.ArgumentParser(description="Hack computer emulator")
    parser.add_argument('inputfile', type=argparse.FileType('r'), default=sys.stdin)

    args = parser.parse_args()

    program = [int(line, 2) for line in args.inputfile]

    if ENABLE_DEBUG:
        hack = DebugComputer(program)
#        hack.add_breakpoint_step(watch_ram(0b10001))
#        hack.add_breakpoint_step(die_when(pc_is(0)))
#        hack.add_breakpoint_step(watch_ram(17))
    elif ENABLE_RECOMPILE_PYTHON:
        hack = RecompilePythonComputer(program)
    else:
        hack = Computer(program)
    hack.run()

    return 0

if __name__ == '__main__':
    main()

# vim: fdm=marker:
