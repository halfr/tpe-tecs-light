#!/usr/bin/env python
#-*- encoding: utf-8 -*-
#
#       utils.py v0.2
#
#       Copyright 2009 Rémi Audebert <quaero.galileo@gmail.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
import time
import operator

# from http://www.daniweb.com/code/snippet305.html
def primes(n):
  """ returns a list of prime numbers from 2 to < n """
  if n < 2:  return []
  if n == 2: return [2]
  # do only odd numbers starting at 3
  s = range(3, n, 2)
  # n**0.5 may be slightly faster than math.sqrt(n)
  mroot = n ** 0.5
  half = len(s)
  i = 0
  m = 3
  while m <= mroot:
    if s[i]:
      j = (m * m - 3)//2
      s[j] = 0
      while j < half:
        s[j] = 0
        j += m
    i = i + 1
    m = 2 * i + 3
  # make exception for 2
  return [2]+[x for x in s if x]

# from http://www.daniweb.com/code/snippet368.html
def print_timing(func):
    def wrapper(*arg, **kwargs):
        t1 = time.time()
        res = func(*arg, **kwargs)
        t2 = time.time()
        print '%s took %0.3f ms' % (func.func_name, (t2-t1)*1000.0)
        return res
    return wrapper

# from http://importantshock.wordpress.com/2006/11/03/one-line-factorial-function-in-python/
def fact(x): return reduce(operator.mul, xrange(2, x+1))

# colors
colors = {'black' : '0;30',
          'bblack': '1;30',
          'red'   : '0;31',
          'bred'  : '1;31',
          'green' : '0;32',
          'bgreen': '1:32',
          'blue'  : '0;34',
          'bblue' : '1;34',
          'white' : '1;37',
          'reset' : '0',}

colors_2 = {
        "default"    :    "\033[0m",
        # style
        "bold"       :    "\033[1m",
        "underline"  :    "\033[4m",
        "blink"      :    "\033[5m",
        "reverse"    :    "\033[7m",
        "concealed"  :    "\033[8m",
        # couleur texte
        "black"      :    "\033[30m", 
        "red"        :    "\033[31m",
        "green"      :    "\033[32m",
        "yellow"     :    "\033[33m",
        "blue"       :    "\033[34m",
        "magenta"    :    "\033[35m",
        "cyan"       :    "\033[36m",
        "white"      :    "\033[37m",
        # couleur fond
        "on_black"   :    "\033[40m", 
        "on_red"     :    "\033[41m",
        "on_green"   :    "\033[42m",
        "on_yellow"  :    "\033[43m",
        "on_blue"    :    "\033[44m",
        "on_magenta" :    "\033[45m",
        "on_cyan"    :    "\033[46m",
        "on_white"   :    "\033[47m",}
 
def c(color):
    try:
        return '\033[%sm' % colors[color]
    except:
        return colors_2[color]

def do_color(color, text):
    #import pdb; pdb.set_trace()
    return c(color)+str(text)+c("reset")

def randlist(k):
    import random
    return random.sample(xrange(10000000), k)
